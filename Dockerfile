FROM node:lts as BUILD_STAGE

RUN apt-get update && apt-get install -y git

WORKDIR /app
COPY package*.json ./
RUN npm ci

COPY . .
ENV NODE_OPTIONS="--openssl-legacy-provider"
RUN npm run build

FROM nginx:mainline-alpine
COPY --from=BUILD_STAGE /app/build /usr/share/nginx/html/esap-gui/
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
