# ESAP-GUI

ESCAPE ESFRI Science Analysis Platform - GUI

This frontend (ReactJS) connects to the ESAP-api-gateway (Django).

## Documentation

Local deployment

```
npm install
npm start
```

## Contributing

For developer access to this repository, please send a message on the[ ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
