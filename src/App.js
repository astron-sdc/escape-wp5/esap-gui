import React from "react";
import "./App.css";
import { BasketContextProvider } from "./contexts/BasketContext";
import { BATCHContextProvider } from "./contexts/BATCHContext";
import { GlobalContextProvider } from "./contexts/GlobalContext";
import { IVOAContextProvider } from "./contexts/IVOAContext";
import { MAQContextProvider } from "./contexts/MAQContext";
import { QueryContextProvider } from "./contexts/QueryContext";
import Routes from "./routes/Routes";

// This is the App, only application global stuff goes here, like the global state provider.
export default function App() {
  return (
    <div>
      <GlobalContextProvider>
        <BasketContextProvider>
          <QueryContextProvider>
            <BATCHContextProvider>
              <IVOAContextProvider>
                <MAQContextProvider>
                  <Routes />
                </MAQContextProvider>
              </IVOAContextProvider>
            </BATCHContextProvider>
          </QueryContextProvider>
        </BasketContextProvider>
      </GlobalContextProvider>
    </div>
  );
}
