import React, { useContext, useEffect } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { NavLink, useHistory } from "react-router-dom";
import esap_logo from "../assets/esap_logo.png";
import { GlobalContext } from "../contexts/GlobalContext";
import { QueryContext } from "../contexts/QueryContext";
import AuthControl from "./auth/authControl";
import MyBasketButton from "./basket/MyBasketButton";
import SaveBasketButton from "./basket/SaveBasketButton";
import ShowTokenButton from "./ShowTokenButton";

export default function NavBar() {
  // History is defined here; since this is the first component that
  // is in the router
  const history = useHistory();

  const { navbar, refreshLogin } = useContext(GlobalContext);

  // On Mount, check login once;
  useEffect(() => {
    refreshLogin(history);
  }, [refreshLogin, history]);

  const { config } = useContext(QueryContext);

  if (!navbar) return null;
  if (!config) return null;



  const navlist = navbar.navbar;

  return (
    <Navbar bg="dark" variant="dark">
      <img
        alt=""
        src={esap_logo}
        height="40"
        className="d-inline-block align-top"
      />

      <Nav className="mr-auto">
        {navlist.map((nav) => (
          <Nav.Link key={nav.title} as={NavLink} to={nav.route}>
            {nav.title}
          </Nav.Link>
        ))}
      </Nav>
      <Nav.Link key="my basket" as={NavLink} to="/basket">
        <MyBasketButton />
      </Nav.Link>
      <SaveBasketButton />{' '}
      <Nav>
        <ShowTokenButton />
        <AuthControl />
      </Nav>
    </Navbar>
  );
}
