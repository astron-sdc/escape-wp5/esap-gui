import React from "react";
import { Button, Card } from "react-bootstrap";
import { NavLink } from "react-router-dom";

import styles from "./ArchiveCard.module.css";


// display a single archive on a card
export default function ArchiveCard({ archive }) {

  return (
    <Card>
      <Card.Body>
        <Card.Title className="h2">{archive.name}</Card.Title>
        <Card className="card-description">
          <Card.Body>
            <img
              className={styles.cardImage}
              alt={`${archive.name} logo`}
              src={archive.thumbnail}
            />
            <Card.Title className="h3 pt-3">
              {archive.short_description}
            </Card.Title>
            <Card.Text className="text-justify">
              {archive.long_description}
            </Card.Text>
          </Card.Body>
        </Card>

        <Button
          className="mt-3"
          as={NavLink}
          variant="outline-primary"
          to={`/archives/${archive.uri}`}
        >
          Visit {archive.name} Archives
        </Button>
      </Card.Body>
    </Card>
  );
}
