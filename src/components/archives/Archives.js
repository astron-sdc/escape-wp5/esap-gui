import React, { useContext } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { GlobalContext } from "../../contexts/GlobalContext";
import ArchiveCard from "./ArchiveCard";

export function Archives() {

  const { archives } = useContext(GlobalContext);
  if (!archives) return null;

  return (
    <Container fluid className="mt-3">
      <Row>
        {
          archives.map(
            (archive) => (
              <Col key={"archive-" + archive.id} >
                <ArchiveCard archive={archive} />
              </Col>
            )
          )
        }
      </Row>
    </Container>
  );
}
