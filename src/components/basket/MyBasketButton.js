import React, { useContext } from "react";
import { Badge, Button } from 'react-bootstrap';
import { BasketContext } from "../../contexts/BasketContext";
import { GlobalContext } from "../../contexts/GlobalContext";
import { getShoppingIcon } from "../../utils/styling";

export default function MyBasketButton(props) {
    const { isAuthenticated } = useContext(GlobalContext);
    const { hasChanged, datasets } = useContext(BasketContext);

    if (isAuthenticated) {
        try {
            let nr_of_items = datasets.length
            if (nr_of_items > 0) {
                if (hasChanged) {
                    return <Button>{getShoppingIcon("must_save_cart")}&nbsp;
                        <Badge variant="light">{nr_of_items}</Badge>
                    </Button>
                } else {
                    return <Button>{getShoppingIcon("cart")}&nbsp;
                        <Badge variant="light">{nr_of_items}</Badge>
                    </Button>
                }
            } else {
                // do not show shopping basket when the basket is empty
                return null
            }

        } catch (e) {
            // do not show shopping basket when there is no basket at all
            return null
        }

    } else {
        // do not show shopping basket when not authenticated
        return null
    }
}
