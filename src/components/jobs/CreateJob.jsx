import React from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import JOB_TYPES from './JobTypes';

/**
 * Component showing the correct job creation form or
 * the list of all available job types.
 *
 * @param {{match: {params: {type: string}}}} props
 * @returns React.FunctionComponent
 */
const CreateJob = (props) => {

    // Route to specific create form
    if (props.match) {
        const type = props.match.params.type;

        if (!(type in JOB_TYPES))
            return <div>Invalid Job Type</div>

        const Component = JOB_TYPES[type].createComponent;
        return (<Component />);
    }

    // Shown on the page
    return (
        <Card style={{ maxWidth: "70rem", margin: "0 auto" }}>
            < Card.Header>
                Create a new Job
            </Card.Header>

            <Card.Body>
                <ListGroup>
                    {Object.keys(JOB_TYPES).map(key =>
                        <ListGroup.Item key={key} action href={`jobs/new/${key}`}>
                            {JOB_TYPES[key].name}
                        </ListGroup.Item>)}
                </ListGroup>
            </Card.Body>
        </Card>
    );
};

export default CreateJob;
