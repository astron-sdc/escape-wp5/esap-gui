import axios from "axios";
import React, { useContext, useEffect, useState } from 'react';
import { GlobalContext } from 'src/contexts/GlobalContext';
import JOB_TYPES from "./JobTypes";
import { reduceUwsKvObjects } from "./util";

function loadJob(jobId, setState, api_host, accessToken) {

    // Ignore loading if accessToken is not valid
    if (accessToken === "" || accessToken.length === 0)
        return;

    setState(prev => ({ ...prev, loading: true }));
    let uri = `${api_host}uws/jobs/${jobId}/`

    axios.get(uri, { headers: { "Authorization": `Bearer ${accessToken}` } })
        .then((res) => {

            const data = res.data;
            setState(prev => ({
                ...prev,
                job: data,
                loading: false,
                error: null,
            }));
        })
        .catch(error => {
            console.log(error);
            setState(prev => ({ ...prev, loading: false, error: error.message }))
        });

}

/**
 * Component showing job details based on the job_id
 *
 * @param {{match: {params: {jobId: string}}}} props
 * @returns React.FunctionComponent
 */
const JobDetail = (props) => {

    const jobId = props.match.params.job_id;

    const { api_host, accessToken } = useContext(GlobalContext);

    const [state, setState] = useState({
        loading: true,
        error: null,
        job: null
    });


    useEffect(() => {
        loadJob(jobId, setState, api_host, accessToken);
    }, [jobId, api_host, accessToken]);


    function refreshJob() {
        loadJob(jobId, setState, api_host, accessToken);
    }

    if (state.loading)
        return (<div>Loading Job Information (ID: {jobId})</div>)

    if (state.error)
        return (<div>Error loading job: <pre>{state.error}</pre></div>)

    const params = reduceUwsKvObjects(state.job.parameters);

    const type = params.type;
    if (!(type in JOB_TYPES))
        return (<div>Invalid Job Type</div>);

    const Component = JOB_TYPES[type].detailComponent;
    return (<Component refreshJob={refreshJob} job={state.job} />);

};

export default JobDetail;
