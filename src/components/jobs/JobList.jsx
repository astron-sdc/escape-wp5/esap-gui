import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { GlobalContext } from "../../contexts/GlobalContext";
import JobListTable from "./JobListTable";

/**
 * Error Component
 * @param {*} props
 * @returns
 */
const ErrorList = (props) => {

    const { message } = props;

    return (
        <>
            <div>An error occurred during loading:</div>
            <pre>{message}</pre>
        </>
    );
}

const JOB_PAGE_SIZE = 10;

/**
 * Load Jobs
 * @param {string} api_host
 * @param {() => void} setState setState function
 * @param {string} accessToken
 * @param {int | undefined} page
 * @returns undefined
 */
function loadJobs(api_host, setState, accessToken, page = undefined) {
    // FIXME: Why is accessToken an empty array by default!?
    if (accessToken === "" || accessToken.length === 0)
        return; // can not perform a request without token

    setState(prev => ({ ...prev, loading: true }))

    let uri = `${api_host}uws/jobs/?page_size=${JOB_PAGE_SIZE}`
    if (page) {
        uri.concat(`&page=${page}`)
    }

    axios.get(uri, { headers: { "Authorization": `Bearer ${accessToken}` } })
        .then((res) => {

            const data = res.data;
            setState(prev => ({
                ...prev,
                jobs: data.results,
                count: data.count,
                page: page ? page : 1,
                pages: data.pages,
                loading: false
            }));
        })
        .catch(error => {
            console.log(error);
            setState(prev => ({ ...prev, loading: false, error: error.message }))
            // TODO: show warning pop-up
        });
}

/**
 * JobList Page
 * Handles loading all available jobs and pagination
 * @returns React.FunctionComponent
 */
const JobList = () => {

    const { api_host, accessToken } = useContext(GlobalContext);

    const [state, setState] = useState({
        jobs: [],
        count: 0,
        page: 1,
        pages: 1,
        loading: true,
        error: null,
    });



    // Load Jobs from API
    useEffect(() => { loadJobs(api_host, setState, accessToken) }, [api_host, accessToken]);

    function handleListRefresh() {
        loadJobs(api_host, setState, accessToken, state.page);
    }

    function handlePage(idx) {
        const page = parseInt(idx); // is a string
        loadJobs(api_host, setState, accessToken, page);
    }

    if (state.loading)
        return (<Container>Loading jobs</Container>);

    if (state.error)
        return (<ErrorList message={state.error} />);

    return <JobListTable
        state={state}
        handleListRefresh={handleListRefresh}
        handlePage={handlePage}
    />
}

export default JobList;
