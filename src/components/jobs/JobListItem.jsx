import { faArchive, faInfo, faPlay, faStop } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../contexts/GlobalContext";
import JobListItemAction from "./JobListItemAction";
import JobStateBadge from "./JobStateBadge";
import { reduceUwsKvObjects } from "./util";

/**
 * An entry in the Job List
 *
 * @param {{job: UWSJob, refreshJobList: () => void}} props
 * @returns React.FunctionComponent
 */
const JobListItem = (props) => {

    const { accessToken } = useContext(GlobalContext);

    const { job, refreshJobList } = props;
    const params = reduceUwsKvObjects(job.parameters);

    /**
     * Event handler for job actions
     * @param {string} phase (either `RUN` or `ABORT`)
     * @param {string} errorMessage descriptive error message
     *
     */
    const handleAction = (phase, errorMessage = "handleAction failed:") => () => {
        axios.post(`${job.url}phase/`, { PHASE: phase }, {
            headers: { "Authorization": `Bearer ${accessToken}` }
        })
            .then(_ => {
                refreshJobList();
            })
            .catch(error => {
                console.log(errorMessage, error);
            });
    }

    function actions(phase) {

        switch (phase) {
            case "PENDING":
            case "HELD":
                return (
                    <JobListItemAction
                        variant="primary"
                        icon={faPlay}
                        text="START"
                        onClick={handleAction("RUN", "handleStart failed:")}
                    />
                );
            case "EXECUTING":
            case "QUEUED":
                return (
                    <JobListItemAction
                        variant="danger"
                        icon={faStop}
                        text="ABORT"
                        onClick={handleAction("ABORT", "handleAbort failed:")}
                    />
                );
            case "ABORTED":
            case "ERROR":
            case "COMPLETED":
                return (
                    <JobListItemAction
                        variant="primary"
                        icon={faArchive}
                        text="ARCHIVE"
                        onClick={handleAction("ARCHIVE", "handleArchive failed:")}
                    />
                )
            default:
                return (<></>);
        }

    }

    return (
        <tr>
            <td><Link to={`/jobs/${job.jobId}/`}>{job.runId}</Link></td>
            <td><JobStateBadge state={job.phase} /></td>
            <td>{new Date(job.creationTime).toLocaleString("default",
                {
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                    timeZone: "utc",
                    timeZoneName: "short"
                })}</td>
            <td>{params.type}</td>
            <td title={JSON.stringify(job.parameters, null, 4)}>
                <abbr>{`${job.parameters.length} parameters`}</abbr>
                &nbsp;
                <FontAwesomeIcon icon={faInfo} size="sm" />
            </td>
            <td title={JSON.stringify(job.results, null, 4)}>
                <abbr>{`${job.results.length} results`}</abbr>
                &nbsp;
                <FontAwesomeIcon icon={faInfo} size="sm" />
            </td>
            <td>{actions(job.phase)}</td>
        </tr>
    )
}

export default JobListItem;
