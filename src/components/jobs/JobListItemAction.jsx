import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import Button from "react-bootstrap/Button";

/**
 * JobListItemAction button.
 *
 * @param {{variant: string, onClick: () => void, icon: IconDefinition, text: string}} props:
 *      variant Color of the button
 *      onClick handler function
 *      icon FontAwesome Icon
 *      text Button text to be displayed
 * @returns React.FunctionComponent
 */
const JobListItemAction = (props) => {
    const { variant, onClick, icon, text } = props;

    return (
        <Button
            style={{ width: "100%" }}
            variant={variant}
            size="sm"
            onClick={() => onClick()}
        >
            <FontAwesomeIcon icon={icon} />
            <span style={{ marginLeft: '0.5rem' }}>{text}</span>
        </Button>
    )
}

export default JobListItemAction;
