import { faRefresh } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import JobListItem from "./JobListItem";

/**
 * View component of a JobList
 *
 * @param {{state: *, handlePage: (page: string) => void, handleListRefresh: () => void}} props:
 *      state of the job list
 *      handlePage function to handle pagination requests
 *      handleListRefresh function to handle a list refresh
 * @returns React.FunctionComponent
 */
const JobListTable = (props) => {

    const { state, handlePage, handleListRefresh } = props;

    return (
        <Container>
            <Row style={{ marginTop: "1rem", marginBottom: "1rem" }}>
                <Col>
                    Jobs: {state.count}
                </Col>
                <Col style={{ textAlign: "center" }}>
                    Page: <select value={state.page} onChange={e => handlePage(e.target.value)}>
                        {
                            Array.from({ length: state.pages }).map(
                                (_, idx) => (<option key={idx} value={idx + 1}>{idx + 1}</option>)
                            )
                        }
                    </select>
                </Col>
                <Col style={{ textAlign: "right" }}>
                    <Button
                        onClick={() => handleListRefresh()}
                        variant="primary"
                        size="sm">

                        <FontAwesomeIcon icon={faRefresh} />
                        <span style={{ marginLeft: '0.5rem' }}>REFRESH</span>
                    </Button>
                </Col>
            </Row>
            <Row>
                <Table>
                    <thead>
                        <tr>
                            <th>Run ID</th>
                            <th>Phase</th>
                            <th>Creation date</th>
                            <th>Type</th>
                            <th>Parameters</th>
                            <th>Results</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {state.jobs.map((job) => <JobListItem
                            key={job.url}
                            job={job}
                            refreshJobList={handleListRefresh} />
                        )}
                    </tbody>
                </Table>
            </Row>
        </Container>
    )
}

export default JobListTable;
