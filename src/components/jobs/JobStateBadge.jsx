import React from "react";
import { Badge } from "react-bootstrap";

/**
 * UWS Job Status Badge
 *
 * @param {state: string} props: job state
 * @returns React.FunctionComponent
 */
const JobStateBadge = (props) => {

    const { state } = props;

    switch (state) {
        case "PENDING":
            return (<Badge variant="warning">PENDING</Badge>);
        case "QUEUED":
            return (<Badge variant="warning">QUEUED</Badge>);
        case "EXECUTING":
            return (<Badge variant="info">EXECUTING</Badge>);
        case "ABORTED":
            return (<Badge variant="danger">ABORTED</Badge>);
        case "COMPLETED":
            return (<Badge variant="success">COMPLETED</Badge>);
        case "HELD":
            return (<Badge variant="warning">HELD</Badge>)
        case "SUSPENDED":
            return (<Badge variant="warning">SUSPENDED</Badge>)
        case "ERROR":
            return (<Badge variant="danger">ERROR</Badge>);
        default:
            return (<Badge variant="light">UNKNOWN</Badge>);
    }

}

export default JobStateBadge;
