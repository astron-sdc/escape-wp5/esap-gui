import CreateJobEcho from "./services/CreateJobEcho";
import CreateJobVOTAP from "./services/CreateJobVOTAP";
import JobDetailEcho from "./services/JobDetailEcho";
import JobDetailVOTAP from "./services/JobDetailVOTAP";

/**
 * List of all defined JOB_TYPES
 */
const JOB_TYPES = Object.freeze({
    "echo": {
        name: "Echo Debug Job",
        createComponent: CreateJobEcho,
        detailComponent: JobDetailEcho,
    },
    "query.vo.tap": {
        name: "VO TAP Query",
        createComponent: CreateJobVOTAP,
        detailComponent: JobDetailVOTAP
    },
});

export default JOB_TYPES;
