import React, { useContext, useState } from 'react';
import { Alert, Button, Card, Form } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import { GlobalContext } from "src/contexts/GlobalContext";
import { submitJob } from "../util";

/**
 * Example create Echo Job form
 *
 * @returns React.FunctionComponent
 */
const CreateJobEcho = () => {

    const { api_host, accessToken } = useContext(GlobalContext);
    const history = useHistory();

    const [state, setState] = useState({
        runId: "",
        message: "",
        loading: false,
        error: null,
        success: false,
    });

    const handleStateChange = (key) => (value) => {
        setState(prev => ({ ...prev, [key]: value }));
    }

    function handleSubmit() {

        const job = {
            runId: state.runId,
            parameters: [
                {
                    key: "type",
                    value: "echo"
                },
                {
                    key: "message",
                    value: state.message
                }
            ]
        }

        setState(prev => ({ ...prev, loading: true }));

        submitJob(api_host, accessToken, job)
            .then(_ => {

                // Clear the input fields
                setState(prev => ({
                    ...prev,
                    runId: "",
                    message: "", loading: false, error: null, success: true }));

                setTimeout(() => {
                    // Redirect to jobs page
                    history.push("/jobs");
                }, 500);
            })
            .catch(error => {
                console.log("job submit failed", error);
                setState(prev => ({ ...prev, loading: false, error: "Could not create job!" }));
            });
    }

    return (
        <Card style={{ maxWidth: "70rem", margin: "0 auto" }}>
            < Card.Header>
                Create a new Echo Job
            </Card.Header>

            <Card.Body>
                {state.success && <Alert variant="success">New Job Created</Alert>}

                <Form>
                    <Form.Group>
                        <Form.Label>Run ID</Form.Label>
                        <Form.Control
                            type="text"
                            value={state.runId}
                            placeholder="Run ID"
                            onChange={e => handleStateChange("runId")(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Message</Form.Label>
                        <Form.Control
                            type="text"
                            value={state.message}
                            placeholder="Message text"
                            onChange={e => handleStateChange("message")(e.target.value)}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Job Type</Form.Label>
                        <Form.Control
                            type="text"
                            value="echo"
                            disabled
                            readOnly
                        />
                    </Form.Group>
                    {
                        state.error &&
                        <Alert variant="warning">
                            {state.error}
                        </Alert>
                    }

                    <Button
                        disabled={state.loading}
                        onClick={handleSubmit}
                    >
                        {state.loading ? "loading..." : "create"}
                    </Button>
                </Form>
            </Card.Body>
        </Card>
    );
};

export default CreateJobEcho;
