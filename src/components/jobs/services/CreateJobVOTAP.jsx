import React, { useContext, useState } from 'react';
import { Alert, Button, Card, Form } from "react-bootstrap";
import { useHistory } from 'react-router-dom';
import { GlobalContext } from "src/contexts/GlobalContext";
import { submitJob } from "../util";


/**
 * Create VO TAP Job Form
 *
 * @returns React.FunctionComponent
 */
const CreateJobVOTAP = () => {

    const { api_host, accessToken } = useContext(GlobalContext);
    const history = useHistory();

    const [state, setState] = useState({
        runId: "",
        service: "https://vo.astron.nl/tap",
        query: "SELECT * FROM ivoa.obscore",
        loading: false,
        error: null,
        success: false,
    });

    const handleStateChange = (key) => (value) => {
        setState(prev => ({ ...prev, [key]: value }));
    }

    function handleSubmit() {

        const job = {
            runId: state.runId,
            parameters: [
                {
                    key: "type",
                    value: "query.vo.tap"
                },
                {
                    key: "service",
                    value: state.service
                },
                {
                    key: "query",
                    value: state.query
                }
            ]
        }

        setState(prev => ({ ...prev, loading: true }));

        submitJob(api_host, accessToken, job)
            .then(_ => {

                // Clear the input fields
                setState(prev => ({
                    ...prev,
                    runId: "",
                    service: "",
                    query: "",
                    loading: false,
                    error: null,
                    success: true
                }));

                // Redirect to jobs page
                setTimeout(() => {
                    history.push("/jobs");
                }, 500);
            })
            .catch(error => {
                console.log("job submit failed", error);
                setState(prev => ({
                    ...prev,
                    loading: false,
                    error: "Could not create job!"
                }));
            });
    }

    return (
        <Card style={{ maxWidth: "70rem", margin: "0 auto" }}>
            < Card.Header>
                Create a new Echo Job
            </Card.Header>

            <Card.Body>
                {state.success && <Alert variant="success">New Job Created</Alert>}

                <Form>
                    <Form.Group>
                        <Form.Label>Run ID</Form.Label>
                        <Form.Control
                            type="text"
                            value={state.runId}
                            placeholder="Run ID"
                            onChange={e => handleStateChange("runId")(e.target.value)}
                            required={true}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Service URL</Form.Label>
                        <Form.Control
                            type="text"
                            value={state.service}
                            placeholder="e.g. https://vo.astron.nl/tap"
                            onChange={e => handleStateChange("service")(e.target.value)}
                            required={true}
                        />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>ADQL Query</Form.Label>
                        <Form.Control
                            type="textarea"
                            value={state.query}
                            placeholder="e.g. SELECT * from ivoa.obscore"
                            onChange={e => handleStateChange("query")(e.target.value)}
                            required={true}
                        />
                    </Form.Group>
                    {
                        state.error &&
                        <Alert variant="warning">
                            {state.error}
                        </Alert>
                    }

                    <Button
                        disabled={state.loading}
                        onClick={handleSubmit}
                    >
                        {state.loading ? "loading..." : "create"}
                    </Button>
                </Form>
            </Card.Body>
        </Card>
    );
};

export default CreateJobVOTAP;
