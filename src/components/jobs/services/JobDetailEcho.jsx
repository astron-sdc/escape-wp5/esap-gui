import React from 'react';

/**
 * Detail Page for Echo Jobs
 *
 * @param {{job: UWSJob}}} props
 * @returns React.FunctionComponent
 */
const JobDetailEcho = (props) => {

    // refreshJob,
    const { job } = props;

    return (<div>
        <table>
            <tbody>
                <tr><td>RunID</td><td>{job.runId}</td></tr>
                <tr><td>Parameters</td><td><pre>{JSON.stringify(job.parameters, null, 2)}</pre></td></tr>
                <tr><td>Results</td><td><pre>{JSON.stringify(job.results, null, 2)}</pre></td></tr>
            </tbody>
        </table>
    </div>);
};

export default JobDetailEcho;
