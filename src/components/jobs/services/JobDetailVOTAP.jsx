import React from 'react';
import AddToBasketCheckBox from 'src/components/basket/AddToBasketCheckBox';
import { reduceUwsKvObjects } from '../util';

function query2basket(job_id, service, query, result) {
    return {
        archive: "esap_async",
        catalog: "query.vo.tap",
        job_id: job_id,
        service: service,
        query: query,
        result: result,
    };
}

/**
 * Detail Page for VO TAP Jobs
 *
 * @param {{job: UWSJob}} props
 * @returns React.FunctionComponent
 */
const JobDetailVOTAP = (props) => {

    // refreshJob,
    const { job } = props;

    // TODO: handle job which is not finished yet.
    const params = reduceUwsKvObjects(job.parameters);
    const results = reduceUwsKvObjects(job.results);

    return (<div>
        <table>
            <tbody>
                <tr><td>RunID</td><td>{job.runId}</td></tr>
                <tr><td>Service</td><td><pre>{params.service}</pre></td></tr>
                <tr><td>Query</td><td><pre>{params.query}</pre></td></tr>
                <tr><td>Results</td><td><a href={results.result_uri}>{results.result_uri}</a></td></tr>
                <tr>
                    <td>Add to basket</td>
                    <td><AddToBasketCheckBox
                        item={query2basket(job.url, params.service, params.query, results.result_uri)} />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>);
}

export default JobDetailVOTAP;
