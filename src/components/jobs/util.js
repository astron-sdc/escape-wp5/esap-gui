import axios from "axios";

/**
 * Reduce UWS key value arrays to single object with the specified keys and values.
 *
 * @param {[{"key": any, "value": any}]} obj UWS Results or Parameters array in form of
 *                [{key: "MyKey", value: "MyValue"}]
 *
 * @returns object mapped such that {MyKey: "MyValue"}
 */
export function reduceUwsKvObjects(obj) {
    return obj.reduce((prev, cur) => {
        let val = { ...prev };
        val[cur.key] = cur.value;
        return val;
    }, {});
}

/**
 * Submit (but not start) an UWS Job
 * @param {string} api_host
 * @param {string} access_token
 * @param {UWSJob} job
 * @returns Promise<AxiosResponse<any>>
 */
export function submitJob(api_host, accessToken, job) {
    if (accessToken === "" || accessToken.length === 0)
        return Promise.reject("Invalid access token");

    return axios.post(`${api_host}uws/jobs/`, job, {
        headers: {"Authorization": `Bearer ${accessToken}`}
    });
}
