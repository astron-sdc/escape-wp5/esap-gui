import { reduceUwsKvObjects } from "./util";

describe("reduceUwsKvObjects", () => {

    it("should handle an empty array", () => {
        const res = reduceUwsKvObjects([]);
        expect(res).toEqual({});
    });

    it("should reduce correctly", () => {
        const res = reduceUwsKvObjects([{ key: "MyKey", value: "MyValue" }]);
        expect(res).toEqual({ MyKey: "MyValue" });
    });

    it("should have `undefined` behaviour for invalid input", () => {
        const res = reduceUwsKvObjects([{ my_key: "MyKey", attr: "MyValue" }]);
        expect(res).toEqual({});
    });

});
