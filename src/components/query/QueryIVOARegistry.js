import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import Form from "react-jsonschema-form";
import "../../assets/IVOA.css";
import { GlobalContext } from "../../contexts/GlobalContext";
import { IVOAContext } from "../../contexts/IVOAContext";
import { QueryContext } from "../../contexts/QueryContext";
import parseQueryForm from "../../utils/form/parseQueryForm";
import parseVOServiceForm from "../../utils/form/parseVOServiceForm";
import { getQueryIcon } from "../../utils/styling";
import LoadingSpinner from "../LoadingSpinner";
import QueryResults from "../services/query_results/QueryResults";
import RunQueryComponent from "./ivoa/RunQuery";




export default function QueryIVOARegistry() {

  const { setConfigName, defaultConf, queryMap, formData, setFormData, page } = useContext(QueryContext);
  const { api_host } = useContext(
    GlobalContext
  );
  const { selectedServices, queryStep, setQueryStep, regPage } = useContext(
    IVOAContext
  );
  const [config, setConfig] = useState();
  const [dplevel] = useState();
  const [collection] = useState();

  const [metadata, setMetadata] = React.useState({});
  const [loading, setLoading] = React.useState(true);


  if (config == null) {
    fetchConfiguration(defaultConf);
  }

  function fetchConfiguration(configName) {

    let configNameString = configName;
    if (configName) {
      configNameString = `?name=${configName}`;
    }

    axios
      .get(api_host + "query/configuration" + configNameString)
      .then((response) => {
        let props = response.data["configuration"].query_schema.properties;

        Object.keys(props).map((key) => {
          if (key === "collection" && collection) {
            props[key]["default"] = collection;
          }
          if (key === "level" && dplevel) {
            props[key]["default"] = dplevel;
          }
          return null;
        });
        setConfig(response.data["configuration"]);
      }).catch((error) => {
        let description = ". Configuration not loaded. Is ESAP-API online? " + api_host
        console.log(error.toString() + description)
        //                alert(description)
        //const loginUrl = api_host + "oidc/authenticate"
        // window.location = loginUrl
        //   loginAgain()
      });
  }

  // set ConfigName for archive
  useEffect(() => {

    setConfigName("esap_ivoa");


    return () => {
      queryMap.clear();
      setFormData();
      setConfigName(defaultConf);
    };

  }, []); // eslint-disable-line react-hooks/exhaustive-deps
  // FIXME: React Hook useEffect has missing dependencies: 'defaultConf', 'queryMap', 'setConfigName', and 'setFormData'. Either include them or remove the dependency array                            react-hooks/exhaustive-deps
  // FIXME: breaks the rules of hooks!

  // load the GUI for this configuration
  useEffect(() => {

    if (!formData) return;

    const gui = config.query_schema.name;
    let queries = [];

    if (queryStep === "run-query") {
      selectedServices.forEach((access_url) => {
        queries = [
          ...queries,
          ...parseVOServiceForm(formData, access_url, page),
        ];
      });
    } else {
      queries = parseQueryForm(gui, formData, regPage);
    }

    queryMap.clear();
    queries.forEach((query) => {
      queryMap.set(query.catalog, {
        catalog: query.catalog,
        service_type: query.service_type,
        esapquery: query.esapquery,
        status: "fetching",
        results: null,
      });
      let url = api_host + "query/" + query.esapquery;
      axios
        .get(url)
        .then((queryResponse) => {
          if (queryStep === "run-query") {
            let vo_table_schema = queryResponse.data.results ? queryResponse.data.results[0] : null;
            let status = queryResponse.data.results ? "fetched" : "error";
            queryMap.set(query.catalog, {
              catalog: query.catalog,
              service_type: query.service_type,
              vo_table_schema: vo_table_schema,
              esapquery: query.esapquery,
              status: status,
              results: queryResponse.data,
            });

          }
          else {
            queryMap.set(query.catalog, {
              catalog: query.catalog,
              service_type: query.service_type,
              esapquery: query.esapquery,
              status: "fetched",
              results: queryResponse.data,
            })
          };
        })
        .catch((error) => {
          queryMap.set(query.catalog, {
            catalog: query.catalog,
            service_type: query.service_type,
            vo_table_schema: "",
            esapquery: query.esapquery,
            status: "error",
            results: [error.message],
          });
        });
    });
  }, [formData, page, regPage]); // eslint-disable-line react-hooks/exhaustive-deps
  // FIXME: React Hook useEffect has missing dependencies: 'api_host', 'config.query_schema.name', 'queryMap', 'queryStep', and 'selectedServices'. Either include them or remove the dependency array  react-hooks/exhaustive-deps
  // FIXME: Break the rules of hooks!





  function formTemplate({ TitleField, properties, title, description }) {
    return (
      <div>
        <TitleField title={title} />
        <div className="row">
          {properties.filter(property => property.content.props.uiSchema["ui:widget"] !== "hidden").map((prop) => (
            <div
              className="col-lg-2 col-md-4 col-sm-6 col-xs-12"
              key={prop.content.key}
            >
              {prop.content}
            </div>
          ))}
        </div>
        {description}
      </div>
    );
  }


  let uiSchemaProp = {}
  if (config) {
    uiSchemaProp = config.ui_schema ? { uiSchema: config.ui_schema } : {};
  } else {
    uiSchemaProp = {};
  }

  if (queryStep === "run-query") {
    return RunQueryComponent({
      config: config,
      uiSchemaProp: uiSchemaProp,
      selectedServices: selectedServices,
      api_host: api_host,
      loading: loading,
      setLoading: setLoading,
      metadata: metadata,
      setMetadata: setMetadata,
      formTemplate: formTemplate,
      formData: formData,
      setFormData: setFormData,
      queryMap: queryMap
    });
  } else {
    if (config) {
      return (
        <Container fluid>
          <Form
            schema={config.query_schema}
            ObjectFieldTemplate={formTemplate}
            formData={formData}
            onSubmit={({ formData }) => setFormData(formData)}
            {...uiSchemaProp}
          >
            <div>
              <Button type="submit">{getQueryIcon()} Query VO Registry</Button>
            </div>
          </Form>
          {Array.from(queryMap.keys()).map((catalog) => {
            // const details = queryMap.get(catalog);
            // let catalogName =
            //   config.query_schema.properties.catalog.enumNames[
            //     config.query_schema.properties.catalog.enum.findIndex(
            //       (name) => name === catalog
            //     )
            //   ];
            return (
              <div key={catalog} className="mt-3">
                <Row>
                  <Col>
                    <h4>List of VO Resources</h4>
                  </Col>
                  <Col>
                    {selectedServices.length === 0 ? (
                      <></>
                    ) : (
                      <Button
                        type="submit"
                        onClick={() => {
                          setQueryStep("run-query");
                        }}
                      >
                        Query selected VO Resources
                      </Button>
                    )}
                  </Col>
                </Row>
                <div className="mt-3">
                  <QueryResults catalog={catalog} />
                </div>
              </div>
            );
          })}
        </Container>
      );
    } else {
      return (<LoadingSpinner />);
    }
  }
}
