import axios from "axios";
import React from "react";
import { Button, Container, Form as RBForm } from "react-bootstrap";
import JSONTree from "react-json-tree";
import Form from "react-jsonschema-form";
import LoadingSpinner from "../../LoadingSpinner";
import VOServiceResults from "../../services/query_results/IVOAResults";

// Color theme for JSON Tree
const COLOR_THEME = {
  scheme: 'monokai',
  author: 'wimer hazenberg (http://www.monokai.nl)',
  base00: '#272822',
  base01: '#383830',
  base02: '#49483e',
  base03: '#75715e',
  base04: '#a59f85',
  base05: '#f8f8f2',
  base06: '#f5f4f1',
  base07: '#f9f8f5',
  base08: '#f92672',
  base09: '#fd971f',
  base0A: '#f4bf75',
  base0B: '#a6e22e',
  base0C: '#a1efe4',
  base0D: '#66d9ef',
  base0E: '#ae81ff',
  base0F: '#cc6633',
};

function RunQueryComponent(props) {

  const { config, uiSchemaProp, selectedServices, loading,
    api_host, metadata, setMetadata, setLoading, formTemplate, formData, setFormData } = props;


  if (config) {
    uiSchemaProp.uiSchema = {
      adql_query: { "ui:widget": "textarea" },
      keyword: { "ui:widget": "hidden" },
      service_type: { "ui:widget": "hidden" },
      catalog: { "ui:widget": "hidden" },
      tap_schema: { "ui:widget": "hidden" },
      waveband: { "ui:widget": "hidden" },
    };

    if (selectedServices && loading) {
      let catalogues = {};
      let counter = 1;
      let selectedServicesLength = selectedServices.length;
      selectedServices.forEach((access_url) => {

        let selectedService = access_url;
        let catalogueTitle = access_url;
        let tf_url = api_host + "query/get-tables-fields/?dataset_uri=vo_reg&access_url=" + selectedService;
        axios
          .get(tf_url)
          .then((tfResponse) => {
            let jsond = tfResponse.data.results;
            let schemas = {};
            Object.keys(jsond).forEach(function (key) {
              let table = {};
              let metadata_couple = jsond[key].table_name.split(/[.]+/);
              let schemaname = metadata_couple[0];
              let tablename = metadata_couple[1];
              table["fields"] = {};
              table["type"] = "table";
              table["full_name"] = jsond[key]["table_name"];
              let fields = jsond[key].fields;
              Object.keys(fields).forEach(function (fieldkey) {
                let fieldname = fields[fieldkey].name;
                table["fields"][fieldname] = fields[fieldkey];
              });

              if (!(schemaname in schemas)) {
                schemas[schemaname] = {};
              }
              schemas[schemaname][tablename] = table;
            });

            catalogues[catalogueTitle] = schemas;
            let metanew = metadata;
            metanew[catalogueTitle] = schemas;
            setMetadata(metanew);
            if (counter >= selectedServicesLength) {
              setLoading(false);
            }
            counter += 1;

          }).catch(error => {
            console.log(error);
          });

      });

    }
    return (
      <Container fluid>
        <Form
          schema={config.query_schema}
          ObjectFieldTemplate={formTemplate}
          formData={formData}
          onSubmit={({ formData }) => setFormData(formData)}
          {...uiSchemaProp}
        >
          <label className="control-label">Selected Services</label>
          <RBForm.Control as="select" className="selectedServices" multiple>
            {selectedServices.map((service) => {
              return <option key="{service}">{service}</option>;
            })}
          </RBForm.Control>


          <div className="metadata-tree">
            <label className="control-label">Service Metadata</label>

            {!loading ?
              <JSONTree data={metadata} theme={COLOR_THEME} invertTheme={true} hideRoot={true} />
              : <LoadingSpinner />}

          </div>

          <div>
            <Button className="mt-3" type="submit">
              Query VO Resource
            </Button>
          </div>
        </Form>
        {selectedServices.map((service) => {
          return (
            <div key="{service}" className="mt-3">
              <VOServiceResults key="{service}" catalog={service} />
            </div>
          );
        })}
      </Container>
    );
  }

}

export default RunQueryComponent;
