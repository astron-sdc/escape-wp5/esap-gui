import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import { Link } from 'react-router-dom';
import "../../assets/Interactive.css";
import { BATCHContext } from "../../contexts/BATCHContext";
import { GlobalContext } from "../../contexts/GlobalContext";

export default function Batch() {

  const { batchSystemURL, setBatchSystemURL, workflowURL, setWorkflowURL, list_of_workflows, setList_of_workflows, list_of_batchSystems, setList_of_batchSystems, list_job_results, setList_job_results } = useContext(BATCHContext);
  const { api_host, accessToken } = useContext(GlobalContext);

  const [defaultWorkflow] = "https://github.com/ESAP-WP5/binder-empty";

  const [state, setState] = useState({
    showDeploy: false,
    showNext: false,
    showMonitor: false,
    showSubmit: false,
    showJobStatus: false,
    showMoreButton: true,
    numberOfitemsShown: 3,
    loading: true
  });

  /* Cannot be included in the above for some reason - breaks */
  const [searchTerm, setSearchTerm] = useState("");

  const [showFacilities, setShowFacilities] = useState(false);





  // Fetch Notebooks
  useEffect(() => {
    axios
      .get(api_host + "batch/workflows/search")
      .then((response) => {
        setList_of_workflows(response.data.results);
        setWorkflowURL(defaultWorkflow);
        setState(prev => ({ ...prev, loading: true }));
      });
  }, [api_host, setList_of_workflows, setWorkflowURL]);

  // Fetch JHubs
  useEffect(() => {
    axios
      .get(api_host + "batch/facilities/search")
      .then((response) => {
        setList_of_batchSystems(response.data.results);
        setBatchSystemURL(response.data.results[0].url);
      });
  }, [api_host, setList_of_batchSystems, setBatchSystemURL]);

  /* Main Monitor Job Button */
  const onClickMonitorJob = e => {
    e.preventDefault();
    setState(prev => ({ ...prev, showSubmit: false, showMonitor: true, showDeloy: false }));
    setShowFacilities(false);
  }

  /* Main Submit Job Button */
  const onClickSubmitJob = e => {
    e.preventDefault();
    setState(prev => ({ ...prev, showNext: false, showSubmit: true, showMonitor: false }));
  }


  const checkJobStatus = uwsJobURL => {
    const headers = {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${accessToken}`
    };
    axios.get(uwsJobURL, { headers })
      .then((response) => {
        setList_job_results(response.data);
        if (response.data.phase === "EXECUTING") setTimeout(checkJobStatus, 1000, response.data.url)
      }).catch(error => {
        console.log("Error when quering batch app api with job number");
      });
  }

  // TODO need to pass dirac job number here (mayb another job number)
  /* Find Job from Monitor page Button */
  /* TODO This will have to go off to the Async and create a ESAP worker job?? */
  const onClickFindJob = e => {
    e.preventDefault();
    const typeParam = {};
    typeParam['key'] = 'type';
    typeParam['value'] = 'batch.query';
    const jobIDparam = {};
    jobIDparam['key'] = 'jobid';
    jobIDparam['value'] = jobValues.jobID;
    const content = {};
    content['runId'] = 'batch';
    content['parameters'] = [typeParam, jobIDparam];
    const headers = {
      'Content-Type': 'application/json',
      "Authorization": `Bearer ${accessToken}`
    };
    axios.post(api_host + "uws/jobs/", content, { headers })
      .then((response) => {
        return axios.post(response.data.url + "phase/", { PHASE: "RUN" }, { headers })
      }).then((response) => {
        setList_job_results(response.data);
        setTimeout(checkJobStatus, 1000, response.data.url)
      }).catch(error => {
        console.log("Error when quering batch app api with job number");
      });
    setState(prev => ({ ...prev, showMonitor: true, showSubmit: false, showJobStatus: true }));
  }

  /* Change handler for Monitor search */
  const handleJobIDChange = e => {
    e.preventDefault();
    setJobValues({ "jobID": e.target.value });
  }

  /* Values for Job Search */
  const [jobValues, setJobValues] = useState({
    jobID: ""
  });

  const setWorkflow = event => {
    setWorkflowURL(event.target.value);
    setState(prev => ({ ...prev, showNext: true }));
    //setShowSkip(false)
  };

  const setFacility = event => {
    setBatchSystemURL(event.target.value);
    setState(prev => ({ ...prev, showDeploy: true }));
  };


  /* TODO This does not work yet but not show in page atm */
  // const showMore = e => {
  //   e.preventDefault();
  //   setState(prev => ({ ...prev, numberOfitemShown: state.numberOfitemShown + 3 }));
  //   if (state.numberOfitemsShown + 3 <= list_of_workflows.length) {
  //     setState(prev => ({ ...prev, numberOfitemShown: state.numberOfitemShown + 3 }));
  //   } else {
  //     setState(prev => ({ ...prev, numberOfitemShown: list_of_workflows.length, showMoreButton: false }));
  //   }
  // }

  const onClickNext = e => {
    e.preventDefault();
    setSearchTerm("");
    setShowFacilities(true);
    setState(prev => ({ ...prev, showSubmit: false }));
  };


  const handleWorkFlowChange = event => {
    setSearchTerm(event.target.value);
    if (event.target.value === "") {
      setState(prev => ({ ...prev, numberOfitemShown: 3, showMoreButton: true }));
    } else {
      setState(prev => ({ ...prev, numberOfitemShown: list_of_workflows.length, showMoreButton: false }));
    }
  }

  const handleFacilityChange = event => {
    setSearchTerm(event.target.value);
    if (event.target.value === "") {
      setState(prev => ({ ...prev, numberOfitemShown: 3, showMoreButton: true }));
    } else {
      setState(prev => ({ ...prev, numberOfitemShown: list_of_workflows.length, showMoreButton: false }));
    }
  }

  const facility_results = !searchTerm
    ? list_of_batchSystems
    : list_of_batchSystems.filter(facility =>
      facility.name.toLowerCase().includes(searchTerm.toLocaleLowerCase())
    );

  let workflow_results = !searchTerm
    ? list_of_workflows
    : list_of_workflows.filter(workflow =>
      ((typeof workflow.name === 'string') && workflow.name.toLowerCase().includes(searchTerm.toLocaleLowerCase())) ||
      ((typeof workflow.keywords === 'string') && workflow.keywords.toLowerCase().includes(searchTerm.toLocaleLowerCase())) ||
      ((typeof workflow.author === 'string') && workflow.author.toLowerCase().includes(searchTerm.toLocaleLowerCase())) ||
      ((typeof workflow.runtimePlatform === 'string') && workflow.runtimePlatform.toLowerCase().includes(searchTerm.toLocaleLowerCase())) ||
      ((typeof workflow.description === 'string') && workflow.description.toLowerCase().includes(searchTerm.toLocaleLowerCase()))
        ((typeof workflow.url === 'string') && workflow.url.toLowerCase().includes(searchTerm.toLocaleLowerCase()))
    );


  //TODO Fancy Stuff Later
  //const workflow_results_sliced = workflow_results?.slice(0, state.numberOfitemsShown) || "";
  const workflow_results_sliced = workflow_results;

  const job_results = list_job_results;

  return (


    <Container className="batch" fluid>

      <h1>Batch Analysis</h1>

      {/* Buttons for Monitoring and Submission */}
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <br /><br />
        <br /><br />
        <Button onClick={onClickMonitorJob} className="search-button">Monitor Batch Jobs</Button>
        <Button onClick={onClickSubmitJob} className="search-button">Submit Batch Jobs </Button>
        <br /><br />
        <br /><br />
        <br /><br />
      </div>

      {/* If Monitor button is pressed then show this part: */}
      {state.showMonitor ?

        <div className="advanced-search">
          <Form className="advanced-form">
            Input Job ID to find information on batch jobs:

            <br /><br />


            <input
              type="text"
              className="advanced-float-left"
              placeholder="Job ID Number"
              onChange={handleJobIDChange}
              autoComplete="off"
            />

            <br /><br />
            <Button onClick={onClickFindJob} className="search-button">Find Batch Jobs </Button>
            <br /><br />

            {/* If find batch jobs button pressed do/show this:  */}
            {/* TODO This will be where the magic happens and information from Async is returned */}
            {state.showJobStatus ?
              <div className="advanced-search">
                Job ID: {jobValues.jobID}
                <br />
                Job Status: {job_results.phase}
                <br />
                Creation Time: {job_results.creationTime}
                <br />
                <br />
                Job Status: {JSON.stringify(job_results.results)}
              </div>
              : null}

            <br /><br />
            <br /><br />
            <br /><br />
          </Form>
        </div>

        : null}

      {/* If Submit button is pressed then show this part: */}
      {state.showSubmit ?

        <div className="advanced-search">
          <Form className="advanced-form">
            <br /><br />
            <input
              className="search-large"
              type="text"
              placeholder="Search for Workflows"
              value={searchTerm}
              onChange={handleWorkFlowChange}
            />

            {state.showNext ?
              <Button className="next-button" onClick={onClickNext}>Next</Button>
              : null}

            <ul className="workflow-ul">
              {workflow_results_sliced.map(item => (
                <li className="workflow-li">
                  <label className="container workflow-checkbox"><input type="radio" name="workflow" onChange={setWorkflow} value={item.url} />  <span className="checkmark"></span></label><h5>{item.name}</h5><br />
                  <span><b>Description: </b> <span dangerouslySetInnerHTML={{ __html: item.description }}></span></span><br />
                  <span><b>Keywords: </b>{item.keywords}</span> <br />
                  <span><b>Steering File: </b>{item.url}</span>

                </li>
              ))}
            </ul>

            {/* Fancy Later TODO
      { state.showMoreButton ?
         <Button className="more-button" onClick={showMore}>More</Button>
       : null }
*/}

            <br /><br />
            <br /><br />
            <br /><br />
          </Form>
        </div>

        : null}


      {showFacilities ?

        <div className="advanced-search">
          <Form className="advanced-form">

            <div className="search-buttons">

              <br /><br />
              <input
                className="search-large"
                type="text"
                placeholder="Search for Facilities"
                value={searchTerm}
                onChange={handleFacilityChange}
              />

              <div className="deploy-buttons">

                {state.showDeploy ?
                  <Link to={{ pathname: "batch/SubmitJob", state: { workflowfile: workflowURL, facility: batchSystemURL } }}>
                    <Button className="deploy-button">Deploy</Button>
                  </Link>
                  //<Button className="deploy-button" href="batch/SubmitJob" >Deploy</Button>
                  //<Button className="deploy-button" href={api_host + "batch/worker/load?workflowfile=" + workflowURL + "&facility=" + batchSystemURL} >Deploy</Button>
                  //<Button className="deploy-button" href={api_host + "batch/CONCORDIA"}>Next</Button>
                  : null}

              </div>

            </div>

            <ul className="facility-ul">
              {facility_results?.map(item => (
                <li className="facility-li">
                  <label className="container facility-checkbox"><input className="radio" onChange={setFacility} name="facility" type="radio" value={item.facilitytype} />  <span className="checkmark"></span></label><h5>{item.name}</h5><br />
                  <span><b>Description:</b> {item.description}</span> <br />
                  <span><b>Runtime Engine: </b>{item.runtimeengine}</span>

                </li>
              ))}
            </ul>

            <br /><br />
            <br /><br />
            <br /><br />
          </Form>
        </div>

        : null}

    </Container>
  );
}
