import React from "react";

const handleFocus = (event) => event.target.select();

export default function BatchParametersComponent(props) {

    const { param, setParam, label } = props;

    return (
            <table width="50%">
                <tr>
                    <td colSpan={200}><label>{label}</label></td>
                    <td align="right"><input value={param} onChange={evt => setParam(evt.target.value)} onFocus={handleFocus}/></td>
                </tr>
            </table>
    );

}
