import React, { useContext } from "react";
import { GlobalContext } from "../../contexts/GlobalContext";
import CreateJob from "../jobs/CreateJob";
import JobList from "../jobs/JobList";

/**
 * Main Job Page
 *
 * Manage a list of active and completed async jobs
 */
const Jobs = () => {

    const { isAuthenticated } = useContext(GlobalContext);

    return (
        isAuthenticated ? (
            <>
                <JobList />
                <CreateJob />
            </>
        ) :
            (
                <div>Please login for job access</div>
            )
    )
}

export default Jobs;
