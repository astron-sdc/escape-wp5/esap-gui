import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Button, Form, Container } from "react-bootstrap";
import { useLocation } from 'react-router-dom';
import { GlobalContext } from "../../contexts/GlobalContext";
import BatchParametersComponent from "./Batch/BatchParametersComponent";

export default function SubmitJob() {

   const { api_host, accessToken } = useContext(GlobalContext);
   const location = useLocation();
   const { workflowfile } = location.state;
   const [jobData, setJobData] = useState(undefined);
   const [params, setParams] = useState({});
   const [state, setState] = useState({
      showSubmitButton: true,
      showRunButton: false,
      showFinal: false,
      url: ""
   });
   const [uwsState, setUwsState] = useState({});
   const [uwsStateRun, setUwsStateRun] = useState([]);

   useEffect(() => {
      axios
         .get(api_host + "batch/worker/load?workflowfile=" + workflowfile)
         .then((response) => {
            const data = response.data;
            // Set initial job data and default parameters
            setJobData(data);
            setParams(data.Parameters);
         })
         .catch(error => {
            console.log("Error when querying batch app api for workflow", error);
         });
   }, [api_host, workflowfile]);

   const onClickSubmitButton = e => {
     e.preventDefault();

     const typeParam = {};
     typeParam['key'] = 'type';
     typeParam['value'] = 'batch.submit';

     const variableParams = paramKeys.map((key,value) => {
        return({key: key, value: params[key]})
     });

     const combinedParams = variableParams.concat(typeParam);

     const content = {};
     content['runId']  = 'batch';
     content['parameters'] = combinedParams;

     const headers = {
         'Content-Type': 'application/json',
         "Authorization": `Bearer ${accessToken}`
     };
     axios.post(api_host + "uws/jobs/", content, { headers })
             .then((response) => {
                const data = response.data;
                setState(prev => ({...prev, url: data.url}));
                setUwsState(data);
         })
         .catch(error => {
            console.log("Error when submitting workflow", error);
         });

     if (state.showRunButton) {
         setState(prev => ({...prev, showRunButton: false, showFinal: false}));
     } else {
         setState(prev => ({...prev, showRunButton: true}));
     };

   };

   const onClickRunButton = e => {
     e.preventDefault();
     const headers = {
         'Content-Type': 'application/json',
         "Authorization": `Bearer ${accessToken}`
     };
     //axios.post(state.url + "phase/", "PHASE=RUN", {headers})
     axios.post(state.url + "phase/", {PHASE:"RUN"}, {headers})
             .then((response) => {
                const data = response.data;
                setUwsStateRun(data);
         })
         .catch(error => {
            console.log("Error when running workflow", error);
         });
     setState(prev => ({...prev, showFinal: true}));
   };

   const onClickAbortButton = e => {
     e.preventDefault();
     const headers = {
         'Content-Type': 'application/json',
         "Authorization": `Bearer ${accessToken}`
     };
     //axios.post(state.url + "phase/", "PHASE=ABORT")
     axios.post(state.url + "phase/", {PHASE:"ABORT"}, {headers})
             .then((response) => {
                const data = response.data;
                setUwsStateRun(data);
         })
         .catch(error => {
            console.log("Error when aborting workflow", error);
         });
     setState(prev => ({...prev, showFinal: true}));
   };


   // This is a curried function which allows
   // setting of a single parameter, but keeps
   // the state of the other parameters the
   const setParameterHandler = (key) => (value) => {
      setParams(prev => ({ ...prev, [key]: value }));
   }

   // Shortcut when data is not yet there
   if (jobData === undefined)
      return (<div>Loading...</div>);

   //const jobKeys = Object.keys(jobData);
   const paramKeys = Object.keys(params);

   // Note the key={key}. This is required in React components whenever
   // you render a list.
   return (

    <div>
    <Container className="batch" fluid>

    <h1>Batch Analysis Submission</h1><br/>
    <h4>{jobData.title}</h4><br/>
    {jobData.description}<br/><br/><br/>

      <Form className="parameters-form">
      {paramKeys.map((key) =>
               <BatchParametersComponent
               key={key}
               label={key}
               param={params[key]}
               setParam={setParameterHandler(key)}
            />
      )}
      </Form><br/>

      { state.showSubmitButton ?
         <Button onClick={onClickSubmitButton} className="submit-button">Submit</Button>
      : null }

      <br/><br/><br/>
      { state.showRunButton ?
         <>
            Job Number: {String(uwsState.url).slice(0,-1).substring(String(uwsState.url).slice(0,-1).lastIndexOf("/")+1,String(uwsState.url).slice(0,-1).length)}
            <br/>
            Status: {uwsState.phase}
            <br/><br/>
            Select RUN to submit the job with the above parameters or ABORT to cancel.<br/>
         </>
      : null }

      <br/><br/><br/>
      { state.showRunButton ?
         <Button onClick={onClickAbortButton} className="run-button">Abort</Button>
      : null }
      {' '}
      { state.showRunButton ?
         <Button onClick={onClickRunButton} className="run-button">Run</Button>
      : null }

      <br/><br/><br/>
      { state.showFinal ?
      <>
         Job Number: {String(uwsState.url).slice(0,-1).substring(String(uwsState.url).slice(0,-1).lastIndexOf("/")+1,String(uwsState.url).slice(0,-1).length)}
         <br/>
         Status: {uwsStateRun.phase}
         <br/><br/>
      </>
      : null }

    </Container>
    </div>
   )
}
