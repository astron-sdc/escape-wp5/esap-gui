import React, { useContext } from "react";
import { Card, Col, Container, Row } from 'react-bootstrap';

import { AladinSimpleContext } from "../../../contexts/AladinSimpleContext";
import LoadingSpinner from '../../LoadingSpinner';

import Aladin from './AladinSimple';

export default function AladinPage(props) {
    const { skyCoords, fov, status, fetchedData } = useContext(AladinSimpleContext);

    let renderPage

    if (status === "fetched") {
        renderPage = <div className="aladin">
            <Aladin ra={skyCoords[0]}
                dec={skyCoords[1]}
                fov={fov}
                data={fetchedData} />
        </div>
    } else {
        renderPage = <LoadingSpinner />
    }

    return (
        <div>
            <Container fluid>
                <Row>

                    <Col sm={9} md={9} lg={9}>
                        <Card>
                            {renderPage}
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
