import React, { useContext } from "react";
import { Card } from 'react-bootstrap';
import { AladinAdvancedContext } from "../../../contexts/AladinAdvancedContext";

import PlanetListCard from './PlanetListCard';
import SearchButton from './SearchButton';
import YearFilterButton from './YearFilterButton';

export default function FilterCard(props) {
    const { filteredDataItems } = useContext(AladinAdvancedContext);

    return (
        <div className="App">
            <Card>

                <Card.Body>
                    <tr><td><h5>Selected Planets: {filteredDataItems.length}</h5></td></tr>
                    <tr><td><YearFilterButton year="All" /></td></tr>
                    <table>
                        <tbody>
                            <tr><td><SearchButton default="search planet name" /></td></tr>
                        </tbody>
                    </table>
                    <PlanetListCard />
                </Card.Body>
            </Card>
        </div>
    );

}
