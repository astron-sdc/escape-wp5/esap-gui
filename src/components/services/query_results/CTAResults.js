import React, { useContext } from "react";
import { Alert } from "react-bootstrap";
import { QueryContext } from "../../../contexts/QueryContext";
import AddToBasket from "../../basket/AddToBasketCheckBox";
import LoadingSpinner from "../../LoadingSpinner";

function projectBasketItem(projectId, category) {
  return {
    archive: "esap_cta",
    catalog: "cta",
    project_id: projectId,
    category: category
  };
}

export default function CTAResults({ catalog }) {
  const { queryMap } = useContext(QueryContext);;

  if (!queryMap) return null;

  if (queryMap.get(catalog).status === "fetched") {

    if (queryMap.get(catalog).results.results.length === 0) {
      return <Alert variant="warning">No matching results found!</Alert>;
    }

    if (catalog === "cta") {

      let cta_url = queryMap.get("cta").results.results[0]['url'];

      return (
        <table>
          <tr>
            <td> <AddToBasket item={projectBasketItem(cta_url)} /> </td>
            <td> <><a href={cta_url}>Download</a></> </td>
          </tr>
        </table>
      );

    }
  }

  return <LoadingSpinner />;

}
