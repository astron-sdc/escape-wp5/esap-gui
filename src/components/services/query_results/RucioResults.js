import axios from "axios";
import React, { useContext } from "react";
import { Alert, Form, Table } from "react-bootstrap";
import { GlobalContext } from "../../../contexts/GlobalContext";
import { QueryContext } from "../../../contexts/QueryContext";
import AddToBasket from "../../basket/AddToBasketCheckBox";
import LoadingSpinner from "../../LoadingSpinner";
import Paginate from "../../Paginate";

function createBasketItem(record) {
  return {
    archive: "rucio",
    record: record,
  };
}

function titleCase(string) {
  var sentence = string.toLowerCase().split(" ");
  for (var i = 0; i < sentence.length; i++) {
    sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
  }
  return sentence.join(" ");
}

export default function RucioResults({ catalog }) {

  const { queryMap } = useContext(QueryContext);
  const { api_host } = useContext(GlobalContext);
  const page = queryMap.get(catalog).page;

  // when the user clicks on the next page the query needs to be redone to ask for the requested page
  function gotoPage(clickEvent) {
    if (clickEvent.target) {

      const newPage = parseInt(clickEvent.target.text);
      if (isNaN(newPage))
      {
        // newPage === NaN when clicking on the current page since the button is disabled in Paginate.js
        // Thus no need to refresh the page.
        return;
      }

      queryMap.set(catalog, {
        catalog: "esap_rucio",
        page: newPage,
        esapquery: queryMap.get(catalog).esapquery + `&page=${newPage}`,
        status: "fetching"
      });

      const url = api_host + "query/query/?" + queryMap.get(catalog).esapquery

      axios
        .get(url, {
          withCredentials: true
        })
        .then((queryResponse) => {
          queryMap.set(catalog, {
            catalog: "esap_rucio",
            page: newPage,
            esapquery: queryMap.get(catalog).esapquery,
            status: "fetched",
            results: queryResponse.data,
          });
        })
        .catch((err) => {

          console.log("fetchRucioPage failed", err);

          queryMap.set(catalog, {
            catalog: "esap_rucio",
            page: newPage,
            esapquery: queryMap.get(catalog).esapquery,
            status: "error",
            results: null,
            error: err
          });

        });

    }

  }

  // If the queryMap does not exist, don't render
  if (!queryMap) {
    return null;
  }

  // Show error
  if (queryMap.get(catalog).status === "error") {
    return <>
      <Alert variant="error">An Error occurred!</Alert>
      <pre>{JSON.stringify(queryMap.get(catalog).error, null, 2)}</pre>
    </>;
  }

  // Show loading spinner
  if (queryMap.get(catalog).status !== "fetched") {
    return <LoadingSpinner />;
  }

  // Show warning when no results are found
  if (queryMap.get(catalog).results.results.length === 0) {
    return <Alert variant="warning">No matching results found!</Alert>;
  }

  // Show Result Table
  if (catalog === "rucio") {

    const numPages = queryMap.get("rucio").results.pages;

    // Construct Header Column names
    const result = queryMap.get("rucio").results.results[0];
    const fields = Object.keys(result);
    const headers = fields.map((field) => {
      const title = titleCase(field.replace("_", " "));
      return <th key={`header_${field}`}>{title}</th>;
    });

    return (
      <>
        <Paginate
          getNewPage={(clickEvent) => gotoPage(clickEvent)}
          currentPage={page}
          numAdjacent={3}
          numPages={numPages}
        />

        <Form>
          <Table className="mt-3" responsive>
            <thead>
              <tr className="bg-light">
                <th>Basket</th>
                {headers}
              </tr>
            </thead>
            <tbody>
              {queryMap.get("rucio")
                .results.results.map((result, resultCounter) => {

                  const cells = fields.map((field) => {
                    const reactKey = `item_${resultCounter}_${field}`;
                    return (
                      <td key={reactKey}>
                        {result[field]}
                      </td>
                    );
                  });

                  return (
                    <tr key={`item_${resultCounter}`}>
                      <td>
                        <AddToBasket id={result.id} item={createBasketItem(result)} />
                      </td>
                      {cells}
                    </tr>
                  );
                })}
            </tbody>
          </Table>
        </Form>

        <Paginate
          getNewPage={(clickEvent) => gotoPage(clickEvent)}
          currentPage={page}
          numAdjacent={3}
          numPages={numPages}
        />
      </>
    );
  }

}
