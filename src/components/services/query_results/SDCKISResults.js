import React, { useContext } from "react";
import { Alert, Table } from "react-bootstrap";
import { QueryContext } from "../../../contexts/QueryContext";
import LoadingSpinner from "../../LoadingSpinner";

export default function SDCKISResults({ catalog }) {

  const { queryMap } = useContext(QueryContext);

  if (!queryMap) return null;

  if (queryMap.get(catalog).status === "fetched") {

    if (queryMap.get(catalog).results.results.length === 0)
      return <Alert variant="warning">No matching results found!  Data Availability: GRIS@GREGOR (2014 - 2021), LARS@VTT (2016 - 2018), ChroTel (2012 - 2020)
      </Alert>;
    else if (catalog === "sdc_kis") {
      const sdckisResults = queryMap.get("sdc_kis").results.results.map((hits) => (
        <>
          <tr>
            <td>{hits.date}</td>
            <td>{hits.time_begin}</td>
            <td>{hits.time_end}</td>
            <td>{hits.target}</td>
            <td>{hits.wave_min} - {hits.wave_max}</td>
            <td>{hits.theta}&deg; (&mu; = {hits.mu})</td>
            <td>
              <a
                href={hits.link}
                target="_blank"
                rel="noopener noreferrer"
              >
                Link
              </a>
            </td>
            <td>
              <a
                href={hits.helio}
                target="_blank"
                rel="noopener noreferrer"
              >
                Link
              </a>
            </td>
          </tr>
        </>
      ));
      return (
        <>
          <Table className="mt-3" responsive>
            <thead>
              <tr className="bg-light">
                <td>Date</td>
                <td>Start time (UTC)</td>
                <td>End time (UTC)</td>
                <td>Target</td>
                <td>Wavelength range (nm)</td>
                <td>Heliocentric angle (&theta;)</td>
                <td>Observation details</td>
                <td>Helioviewer</td>
              </tr>
            </thead>
            <tbody>
              {sdckisResults}
            </tbody>
          </Table>
        </>

      );

    }
  }
  else {
    return <LoadingSpinner />;
  }
}
