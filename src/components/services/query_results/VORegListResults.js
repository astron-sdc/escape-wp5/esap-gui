import React, { useContext } from "react";
import { Alert, InputGroup, Table } from "react-bootstrap";
import { IVOAContext } from "../../../contexts/IVOAContext";
import { QueryContext } from "../../../contexts/QueryContext";
import LoadingSpinner from "../../LoadingSpinner";
import Paginate from "../../Paginate";

export default function VORegListResults({ catalog }) {

  const { queryMap } = useContext(QueryContext);
  const {
    addRegistry,
    removeRegistry,
    regPage,
    setRegPage,
  } = useContext(IVOAContext);

  if (!queryMap) return null;

  if (queryMap.get(catalog).status === "fetched") {

    if (queryMap.get(catalog).results.results.length === 0)
      return <Alert variant="warning">No matching results found!</Alert>;

    // Show results
    const numPages = queryMap.get(catalog).results.pages;

    // FIXME: implement pagination!

    return (
      <>
        <Paginate
          getNewPage={(args) => {
            return args.target
              ? setRegPage(parseFloat(args.target.text))
              : null;
          }}
          currentPage={regPage}
          numAdjacent={3}
          numPages={numPages}
        />

        <Table className="mt-3" responsive>
          <thead>
            <tr className="bg-light">
              <th>
                <InputGroup>
                  <InputGroup.Checkbox
                  // onChange={(event) => {
                  //   setCheckAll(event.target.checked);
                  //   event.target.checked
                  //     ? queryMap
                  //         .get(catalog)
                  //         .results.results.map((result) => {
                  //           addRegistry(result.access_url);
                  //         })
                  //     : queryMap
                  //         .get(catalog)
                  //         .results.results.map((result) => {
                  //           removeRegistry(result.access_url);
                  //         });
                  // }}
                  />
                </InputGroup>
              </th>
              <th>Resource</th>
              <th>Access URL</th>
              <th>Waveband</th>
              <th>Title</th>
              <th>Service Type</th>
              <th>Content Types</th>
            </tr>
          </thead>
          <tbody>
            {queryMap.get(catalog).results.results.map((result, idx) => {
              return (
                <tr key={idx}>
                  <th>
                    <InputGroup>
                      <InputGroup.Checkbox
                        // checked={checkAll}
                        onChange={(event) => {
                          event.target.checked
                            ? addRegistry(result.access_url)
                            : removeRegistry(result.access_url);
                        }}
                      />
                    </InputGroup>
                  </th>

                  <td>{result.short_name}</td>
                  <td>{result.access_url}</td>
                  <td>{result.waveband}</td>
                  <td>{result.title}</td>
                  <td>{result.service_type}</td>
                  <td>{result.content_types}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </>
    );
  } else {
    return <LoadingSpinner />;
  }
}
