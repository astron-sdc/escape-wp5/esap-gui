import React from 'react';
import { Table } from "react-bootstrap";
import AddToBasket from "../../basket/AddToBasketCheckBox";

function SAMPBasketItem(record) {
    return {
        archive: "samp",
        record: record,
    };
}

export default function SampResults(props) {

    let fieldnames = props.fieldnames
    let data = props.votable_in_json['data']

    return (
        <>
            <Table className="mt-3" responsive>
                <thead>

                    <tr className="bg-light">
                        <th>Basket</th>
                        {fieldnames.map((field) => {
                            return (
                                <th key={field}>{field}</th>
                            );
                        })}

                    </tr>

                </thead>
                <tbody>

                    {data.map((record, index) => {

                        let id = `samp${index}`
                        // FIXME: support more then 1000 rows
                        if (index > 1000) {
                            return null;
                        }
                        return (
                            <tr key={record}>
                                <td>
                                    <AddToBasket id={id} item={SAMPBasketItem(record)} />
                                </td>
                                {record.map((col) => {
                                    let value = 'n/a'

                                    try {
                                        value = col.toString()
                                    } catch (e) {
                                    }

                                    if (value.includes('http')) {
                                        value = <a href={value} target="_blank" rel="noopener noreferrer">{value}</a>
                                    }
                                    return (
                                        <td key={value}>{value}</td>
                                    )
                                })}
                            </tr>
                        );
                    })}
                </tbody>

            </Table>
        </>
    );

}
