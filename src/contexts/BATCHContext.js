import React, { createContext, useState } from "react";


export const BATCHContext = createContext();
export function BATCHContextProvider({ children }) {

    const [batchSystemURL, setBatchSystemURL] = useState();
    const [workflowURL, setWorkflowURL] = useState();
    const [batchSystemsURL, setBatchSystemsURL] = useState();
    const [list_of_workflows, setList_of_workflows] = useState();
    const [list_of_batchSystems, setList_of_batchSystems] = useState();
    const [list_job_results, setList_job_results] = useState([]);

    return (
        <BATCHContext.Provider
            value={{
                batchSystemURL,
                setBatchSystemURL,
                workflowURL,
                setWorkflowURL,
                batchSystemsURL,
                setBatchSystemsURL,
                list_of_workflows,
                setList_of_workflows,
                list_of_batchSystems,
                setList_of_batchSystems,
                list_job_results,
                setList_job_results
            }}
        >
            {children}
        </BATCHContext.Provider>
    )
}
