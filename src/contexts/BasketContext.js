import axios from "axios";
import React, { createContext, useContext, useEffect, useState } from "react";
import { GlobalContext } from "./GlobalContext";

export const BasketContext = createContext();

function ResponseToDatasets(response) {
  // the reponse has an extra level 'item_data', because that is how the backend serializer works
  // The BasketContext.datasets does not have that level and is simply an array of contents
  let shopping_cart = response.data.shopping_cart

  let datasets = shopping_cart.map((item) => {
    // make item_data an object instead of a string
    return JSON.parse(item.item_data)
  });

  return datasets
}


export function BasketContextProvider({ children }) {

  const { isAuthenticated, api_host } = useContext(GlobalContext);

  const [datasets, setDatasets] = useState([]);

  const [hasChanged, setHasChanged] = useState(false);
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {
    if (!isAuthenticated)
      return;

    const profileUrl = api_host + "accounts/user-profiles/";

    function getUserProfile() {
      return axios.get(profileUrl, { withCredentials: true })
    }

    function getDatasets(response) {
      const userProfileUrl = profileUrl + response.data.results[0].user_name + "/";
      return axios.get(userProfileUrl, { withCredentials: true })
    }

    getUserProfile()
      .then(res => getDatasets(res))
      .then(res => {
        // load the shopping_cart into the basketContext
        setDatasets(ResponseToDatasets(res))
      })
      .catch(error => {
        console.log(error);
      });

  }, [isAuthenticated, api_host, setDatasets]);


  function handleAddDataset(dataset) {
    setDatasets([...datasets, dataset]);
    setHasChanged(true)
  }

  function handleRemoveDataset(dataset) {
    const copy = [...datasets];
    const index = copy.findIndex((ds) => ds === dataset);
    copy.splice(index, 1);
    setDatasets(copy);
    setHasChanged(true)
  }

  function handleClearDatasets() {
    const copy = []
    setDatasets(copy);
    setHasChanged(true)
  }

  return (
    <BasketContext.Provider
      value={{
        datasets, setDatasets,
        hasChanged, setHasChanged,
        refresh, setRefresh,
        add: handleAddDataset,
        remove: handleRemoveDataset,
        clear: handleClearDatasets,
        changed: setHasChanged,
      }}
    >
      {children}
    </BasketContext.Provider>
  );
}
