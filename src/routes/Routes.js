import React, { useContext } from "react";
import { Route, Switch } from "react-router-dom";

import { BrowserRouter as Router } from "react-router-dom";
import CreateJob from "src/components/jobs/CreateJob";
import JobDetail from "src/components/jobs/JobDetail";
import ArchiveDetails from "../components/archives/ArchiveDetails";
import { Archives } from "../components/archives/Archives";
import MyBasketPage from "../components/basket/MyBasketPage";
import NavBar from "../components/NavBar";
import QueryCatalogs from "../components/query/QueryCatalogs";
import QueryIVOARegistry from "../components/query/QueryIVOARegistry";
import QueryMultipleArchives from "../components/query/QueryMultipleArchives";
import AladinAdvancedPage from '../components/services/aladin/AladinAdvancedPage';
import AladinSimplePage from '../components/services/aladin/AladinSimplePage';
import Batch from "../components/services/Batch";
import Interactive from "../components/services/Interactive";
import Jobs from "../components/services/Jobs";
import Rucio from "../components/services/Rucio";
import SampPage from '../components/services/samp/SampPage';
import SubmitJob from "../components/services/SubmitJob";
import { AladinAdvancedContextProvider } from "../contexts/AladinAdvancedContext";
import { AladinSimpleContextProvider } from "../contexts/AladinSimpleContext";
import { GlobalContext } from "../contexts/GlobalContext";
import { IVOAContextProvider } from "../contexts/IVOAContext";
import { COMMIT_DATE, COMMIT_URL } from "../utils/version";

export default function Routes() {
    const { navbar, handleLogin, handleLogout, handleError } = useContext(GlobalContext);

    if (!navbar) return null;

    return (

        <Router basename={navbar.frontend_basename}>

            <NavBar />
            <Switch>
                <Route exact path={["/", "/archives"]}>
                    <Archives />
                </Route>
                <Route exact path="/rucio">
                    <Rucio />
                </Route>
                <Route exact path="/interactive">
                    <Interactive />
                </Route>

                {/* Specific pages for submitting a new job */}
                <Route
                    path="/jobs/new/:type/"
                    render={(props) => <CreateJob {...props} />}
                />

                {/* To interact with a given Job */}
                <Route
                    path="/jobs/:job_id/"
                    render={(props) => <JobDetail {...props} />}
                />

                {/* Main job list */}
                <Route exact path="/jobs">
                    <Jobs />
                </Route>

                <Route exact path="/batch">
                    <Batch />
                </Route>

                <Route exact path="/batch/SubmitJob">
                    <SubmitJob />
                </Route>

                <Route exact path="/login" component={handleLogin} />
                <Route exact path="/logout" component={handleLogout} />
                <Route exact path="/error" component={handleError} />

                <Route exact path="/archives/:uri" component={ArchiveDetails} />

                {/* specific behaviour for IVOA 'archive' to enable a 2-stage query */}
                <Route exact path="/archives/ivoa/query">
                    <IVOAContextProvider>
                        <QueryIVOARegistry />
                    </IVOAContextProvider>
                </Route>


                {/* default 1-stage synchronous query behaviour for most archives */}
                <Route exact path={["/archives/:uri/query"]}>
                    <QueryCatalogs />
                </Route>

                {/* query multiple archives */}
                <Route exact path="/query">
                    <QueryMultipleArchives />
                </Route>

                <Route exact path="/samp"  >
                    <SampPage />
                </Route>

                <Route exact path="/aladin_simple"  >
                    <AladinSimpleContextProvider>
                        <AladinSimplePage />
                    </AladinSimpleContextProvider>
                </Route>

                <Route exact path="/aladin_advanced"  >
                    <AladinAdvancedContextProvider>
                        <AladinAdvancedPage />
                    </AladinAdvancedContextProvider>
                </Route>

                <Route exact path="/basket"  >
                    <MyBasketPage />
                </Route>

            </Switch>

            <footer><small><a href={COMMIT_URL}>ESAP-GUI version {COMMIT_DATE}</a></small></footer>
        </Router>
    );
}
