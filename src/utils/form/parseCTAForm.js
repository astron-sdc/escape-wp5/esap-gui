export default function parseCTAForm(formData) {
  let formInput = Object.entries(formData);

  let query = "";

  for (let [key, value] of formInput) {
    query += `${`${query}` ? "&" : ""}` + key + "=" + value;
  }

  let esapquery = [
    query,
    "archive_uri=esap_cta",
    `catalog=cta`,
  ].join("&");

  return [{
    catalog: "cta",
    esapquery: esapquery
  }];
}
