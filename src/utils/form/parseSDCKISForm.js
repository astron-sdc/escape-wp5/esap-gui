export default function parseSDCKISForm(formData) {
  let formInput = Object.entries(formData);

  let query = "";

  for (let [key, value] of formInput) {
    query += `${`${query}` ? "&" : ""}` + key + "=" + value;
  }

  let esapquery = [
    query,
    "archive_uri=sdc_kis",
    `catalog=sdc_kis`,
  ].join("&");

  console.log("SDC KIS Query", query);
  return [{
    catalog: "sdc_kis",
    esapquery: esapquery
  }];
}
